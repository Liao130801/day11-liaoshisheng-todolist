import React, {useState} from "react";
import TodoGenerator from "./TodoGenerator";
import TodoGroup from "./TodoGroup";

const TodoList = () => {
    const [todos, setTodos] = useState([]);

    const handleDeleteTodo = (index) => {
        const updatedTodos = todos.filter((_, i) => i !== index);
        setTodos(updatedTodos);
    };

    const handleAddTodo = (todo) => {
        setTodos([...todos, todo]);
    };

    return (
        <div className="todolist">
            <TodoGenerator onAddTodo={handleAddTodo} />
            <TodoGroup todos={todos} onDeleteTodo={handleDeleteTodo} />
        </div>
    );
};

export default TodoList;