import React from "react";
import TodoItem from "./TodoItem";
const TodoGroup = ({ todos, onDeleteTodo }) => {
    return (
        <ul>
            {todos.map((todo, index) => (
                <TodoItem key={index} todo={todo} onDelete={() => onDeleteTodo(index)} />
            ))}
        </ul>
    );
};
export default TodoGroup;