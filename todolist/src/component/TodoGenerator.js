import React, {useState} from "react";
import { Button ,Space,Input} from 'antd';

const TodoGenerator = ({ onAddTodo }) => {
    const [inputValue, setInputValue] = useState('');

    const handleChange = (e) => {
        setInputValue(e.target.value);
    };

    const handleAddTodo = () => {
        if (inputValue.trim() !== '') {
            onAddTodo(inputValue);
            setInputValue('');
        }
    };

    return (
        <div>
            <Space>
            <Input type="text" value={inputValue} onChange={handleChange} style={{ width: 200 }}/>
            <Button onClick={handleAddTodo}>Add</Button>
            </Space>
        </div>
    );
};
export default TodoGenerator;