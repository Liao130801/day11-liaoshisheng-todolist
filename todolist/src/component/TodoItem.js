import React from "react";
import { Button,Space,Card} from 'antd';

const TodoItem = ({ todo, onDelete }) => {
    return (
        <li>
            <Space>
                <Card style={{ width: 300 }}>
                    {todo}
                </Card>
            <Button onClick={onDelete}>Delete</Button>
            </Space>
        </li>
    );
};
export default TodoItem;