
import './App.css';
import TodoList from "./component/TodoList";
import React from "react";

function App() {
  return (
    <div className="App">
       <TodoList/>
    </div>
  );
}

export default App;
